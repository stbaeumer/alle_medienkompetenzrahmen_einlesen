﻿using System.Collections.Generic;

namespace alle_medienkompetenzrahmen_einlesen
{
    public class Zeile
    {
        public Zeile()
        {
            this.Zellen = new List<Zelle>();
        }

        public string Bildungsgang { get; internal set; }
        public List<Zelle> Zellen { get; internal set; }
    }
}