﻿using System.Collections.Generic;

namespace alle_medienkompetenzrahmen_einlesen
{
    public class Links :List<Link>
    {
        public Links()
        {
            this.Add(new Link("Padlet", "https://bk-borken.padlet.org/"));
            this.Add(new Link("Oncoo", "https://oncoo.de/"));
            this.Add(new Link("Moodle", "https://bk-borken.lms.schulon.org/login/index.php"));
            this.Add(new Link("Mentimeter", "https://bk-borken.padlet.org/"));
            this.Add(new Link("Lucidchart", "https://bk-borken.padlet.org/"));
            this.Add(new Link("Bitbucket", "https://bitbucket.org/product/"));
            this.Add(new Link("Lucidchart", "https://www.lucidchart.com/pages/"));
            this.Add(new Link("PGP", "https://www.gpg4win.de/"));
            this.Add(new Link("Webuntis", "https://nessa.webuntis.com/WebUntis/?school=bk-borken#/basic/main"));
            this.Add(new Link("Messengers", "https://nessa.webuntis.com/WebUntis/?school=bk-borken#/basic/main"));
            this.Add(new Link("Messenger", "https://nessa.webuntis.com/WebUntis/?school=bk-borken#/basic/main"));            
            this.Add(new Link("Zenkit", "https://zenkit.com/"));
            this.Add(new Link("Webweavers", "https://www.krbor.de/wws/100001.php"));
            this.Add(new Link("Webweaver", "https://www.krbor.de/wws/100001.php"));
            this.Add(new Link("BKB-TV", "https://vimeo.com/bkbtv"));
            this.Add(new Link("Instagram", "https://www.instagram.com/berufskolleg_borken/"));
            this.Add(new Link("Quizlet", "https://quizlet.com/de"));
            this.Add(new Link("Youtube", "https://www.youtube.com"));
            this.Add(new Link("Office365", "https://www.office.com"));
            this.Add(new Link("Office 365", "https://www.office.com"));
            this.Add(new Link("Office", "https://www.office.com"));
            this.Add(new Link("Word", "https://www.office.com"));
            this.Add(new Link("Excel", "https://www.office.com"));
            this.Add(new Link("Powerpoint", "https://www.office.com"));
            this.Add(new Link("Sway", "https://www.office.com"));            
            this.Add(new Link("eTwinning", "https://www.etwinning.net/de/pub/index.htm"));
            this.Add(new Link("Explainity", "https://www.explainity.de/"));
            this.Add(new Link("uitmuntend", "https://www.uitmuntend.de/"));
            this.Add(new Link("Linguee", "https://www.linguee.de/"));
            this.Add(new Link("dict.cc", "https://www.dict.cc/"));
            this.Add(new Link("Kahoot", "https://kahoot.com/"));
            this.Add(new Link("Keller", "https://cnc-keller.de/"));
            this.Add(new Link("Kellersoftware", "https://cnc-keller.de/"));
            this.Add(new Link("BYOD", "https://de.wikipedia.org/wiki/Bring_your_own_device"));
            //this.Add(new Link("", ""));
            //this.Add(new Link("", ""));
            //this.Add(new Link("", ""));
            //this.Add(new Link("", ""));


        }
    }
}