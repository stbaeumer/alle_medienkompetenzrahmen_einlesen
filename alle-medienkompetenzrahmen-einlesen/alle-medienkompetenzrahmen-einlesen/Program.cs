﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace alle_medienkompetenzrahmen_einlesen
{
    class Program
    {
        public static string Ordner = @"C:\Users\bm\Berufskolleg Borken\Medienkonzept - Documents\Medienkompetenzrahmen\";

        public static string[,] Matrix = new string[4, 6];

        static void Main(string[] args)
        {
            Matrix[0, 0] = "1.1 Medienausstattung (Hardware)";
            Matrix[0, 1] = "2.1 Informationsrecherche";
            Matrix[0, 2] = "3.1 Kommunikationsund Kooperationsprozesse";
            Matrix[0, 3] = "4.1 Medienproduktion und Präsentation";
            Matrix[0, 4] = "5.1 Medienanalyse";
            Matrix[0, 5] = "6.1 Prinzipien der digitalen Welt";

            Matrix[1, 0] = "1.2 Digitale Werkzeuge";
            Matrix[1, 1] = "2.2 Informationsauswertung";
            Matrix[1, 2] = "3.2 Kommunikationsund Kooperationsregeln";
            Matrix[1, 3] = "4.2 Gestaltungsmittel";
            Matrix[1, 4] = "5.2 Meinungsbildung";
            Matrix[1, 5] = "6.2 Algorithmen erkennen";

            Matrix[2, 0] = "1.3 Datenorganisation";
            Matrix[2, 1] = "2.3 Informationsbewertung";
            Matrix[2, 2] = "3.3 Kommunikation und Kooperation in der Gesellschaft";
            Matrix[2, 3] = "4.3 Quellendokumentation";
            Matrix[2, 4] = "5.3 Identitätsbildung";
            Matrix[2, 5] = "6.3 Modellieren und Programmieren";

            Matrix[3, 0] = "1.4 Datenschutz und Informationssicherheit";
            Matrix[3, 1] = "2.4 Informationskritik";
            Matrix[3, 2] = "3.4 Cybergewalt und -kriminalität";
            Matrix[3, 3] = "4.4 Rechtliche Grundlagen";
            Matrix[3, 4] = "5.4 Selbstregulierte Mediennutzung";
            Matrix[3, 5] = "6.4 Bedeutung von Algorithmen";

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Documents docs = app.Documents;

            List<Zeile> excel = new List<Zeile>();

            Links links = new Links();

            DirectoryInfo di = new DirectoryInfo(Ordner);

            int numDocx = di.GetFiles("*.docx", SearchOption.AllDirectories).Length;
            int x = 1;
            Zeile bildungsgangFach = new Zeile(); 

            foreach (var datei in System.IO.Directory.GetFiles(Ordner, "*.docx").OrderBy(f => f))
            {               
                if (!Path.GetFileName(datei).StartsWith("~") && Path.GetFileName(datei).StartsWith("_"))
                {
                    Console.WriteLine("");
                    Console.WriteLine(x + "/" + numDocx + " " + datei);

                    try
                    {
                        Document doc = docs.Open(datei, ReadOnly: true);
                        Table t = doc.Tables[1];
                        Microsoft.Office.Interop.Word.Range r = t.Range;
                        Cells cells = r.Cells;

                        bildungsgangFach = new Zeile();

                        for (int i = 1; i <= cells.Count; i++)
                        {
                            Cell cell = cells[i];
                            Microsoft.Office.Interop.Word.Range r2 = cell.Range;
                            String inhalt = r2.Text;
                            inhalt = InhaltAufereiten(inhalt);

                            if (inhalt.Contains("Medienkompetenzrahmen NRW: Umsetzung im Bildungsg"))
                            {                                
                                bildungsgangFach.Bildungsgang = inhalt.Replace("Medienkompetenzrahmen NRW: Umsetzung im Bildungsgang", "").Trim().Replace("\r\a", "");
                                Console.WriteLine("");
                                Console.WriteLine(bildungsgangFach.Bildungsgang);
                                Console.WriteLine("===============================================================================");
                                x++;
                            }
                            else
                            {
                                if (IstRelevant(inhalt, Matrix))
                                {
                                    bildungsgangFach.Zellen.Add(new Zelle(inhalt, bildungsgangFach.Bildungsgang, Matrix, links));
                                    if (bildungsgangFach.Bildungsgang == null)
                                    {
                                        string aa = "";
                                    }
                                }

                                Marshal.ReleaseComObject(cell);
                                Marshal.ReleaseComObject(r2);
                            }
                        }

                        excel.Add(bildungsgangFach);
                    }
                    catch (Exception ex)
                    {
                        if (ex.ToString().Contains("angeforderte Element"))
                        {
                            Console.WriteLine(ex.ToString());
                            Console.WriteLine("Das Worddokument enthält keine lesbare Tabelle.");
                        }
                        else
                        {
                            Console.WriteLine(ex.ToString());
                            Console.ReadKey();
                            Environment.Exit(0);
                        }
                    }

                    CopyDatei(datei, bildungsgangFach);
                    
                }
            }
            
            Console.WriteLine("Jetzt wird nach Excel geschrieben ...");
           
            foreach (var item in new List<int>() { 1, 2, 3, 4 })
            {
                string xlsxDatei = Ordner + "MKR" + item + ".xlsx";

                Console.Write(xlsxDatei + " ...");

                Microsoft.Office.Interop.Excel.Application xlsx = new Microsoft.Office.Interop.Excel.Application();
                Workbook workbook = xlsx.Workbooks.Open(xlsxDatei);
                Worksheet worksheet = (Worksheet)workbook.Worksheets.get_Item(1);
                Microsoft.Office.Interop.Excel.Range xlRange = worksheet.UsedRange;

                try
                {
                    // Finde erste leere Zeile

                    xlRange = (Microsoft.Office.Interop.Excel.Range)worksheet.Cells[worksheet.Rows.Count, 1];
                    long lastRow = (long)xlRange.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlUp).Row;
                    long newRow = 3;// lastRow + 1;

                    // Schreibe alle Zeilen, die zur Zeile 4 gehören.

                    foreach (var bgFach in excel)
                    {
                        foreach (var zelle in bgFach.Zellen)
                        {
                            if (zelle.Zeile == item)
                            {
                                worksheet.Cells[newRow, zelle.Spalte] = zelle.Inhalt;
                                if (zelle.Spalte == 6)
                                {
                                    newRow++;
                                }
                            }
                        }
                    }


                    xlsx.Visible = false;
                    xlsx.UserControl = false;
                    workbook.SaveAs(xlsxDatei, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                        false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    workbook.Close();
                    xlsx.Quit();
                    Console.WriteLine(" .. OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    workbook.Close(0);
                    xlsx.Quit();
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }
            Console.WriteLine("Verarbeitung beendet. Programm schließen.");
            Console.ReadKey();
        }

        private static void CopyDatei(string datei, Zeile bildungsgangFach)
        {
            var dateiOhneExtension = Path.GetFileNameWithoutExtension(datei);
            var xx = string.Concat(bildungsgangFach.Bildungsgang.Split(Path.GetInvalidFileNameChars()));
            var zz = xx;
            var neu = datei.Replace(dateiOhneExtension, ("_BKB_MKR_" + xx.Replace(",", "_").Replace(".", "_")).Replace(" ", "").Replace(":", "_").Replace("/", "_").Replace("\\", "_").Replace(@"\a", "").Replace("\"", ""));

            if (!dateiOhneExtension.StartsWith("_"))
            {
                try
                {
                    System.IO.File.Copy(datei, neu);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    
                }
                
            }            
        }
                

        private static bool IstRelevant(string inhalt, string[,] matrix)
        {
            if(inhalt.StartsWith("1.") || inhalt.StartsWith("2.") || inhalt.StartsWith("3.") || inhalt.StartsWith("4.") || inhalt.StartsWith("5.") || inhalt.StartsWith("6."))
            {
                return true;
            }
            else
            {
                return false;
            }            
        }

        private static string InhaltAufereiten(string txt)
        {
            // String zerlegen ...

            List<string> v = new List<string>();

            foreach (var item in txt.Split(' '))
            {
                // ... aufbereiten

                var it = item.Replace("-\v", "-");

                if (it != "" && it != "\v")
                {
                    v.Add(it.Trim());
                }
            }

            // String zusammensetzen

            string inhalt = "";

            foreach (var item in v)
            {
                inhalt += (item.EndsWith("-") ? item.Substring(0,item.Length - 1) : item + " ");
            }
            return inhalt.Trim();
        }
    }
}