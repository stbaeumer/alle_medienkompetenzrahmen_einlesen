﻿namespace alle_medienkompetenzrahmen_einlesen
{
    public class Link
    {
        public string Hyperlink { get; private set; }
        public string Name { get; private set; }

        public Link(string name, string l)
        {
            Name = name;
            Hyperlink = l;
        }
    }
}