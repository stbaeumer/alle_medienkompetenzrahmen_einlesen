﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace alle_medienkompetenzrahmen_einlesen
{
    public class Zelle
    {
        private string[,] matrix;

        public int Zeile { get; private set; }
        public int Spalte { get; private set; }
        public string Inhalt { get; private set; }
        
        public Zelle(string inhalt, string bildungsgang, string[,] matrix, Links links)
        {
            Inhalt = inhalt;
            this.matrix = matrix;
                        
            for (int z = 0; z < 4; z++)
            {
                for (int s = 0; s < 6; s++)
                {
                    var m = matrix[z, s];
                    if (inhalt.Contains(m))
                    {
                        inhalt = inhalt.Replace(m, "");
                        inhalt = inhalt.Replace("\r\a", "</br>").Replace("\r\r\r\r", "\r");
                        inhalt = inhalt.Replace("\r\a", "</br>").Replace("\r\r\r", "\r");
                        inhalt = inhalt.Replace("\r\a", "</br>").Replace("\r\r", "\r");
                        inhalt = inhalt.Replace("\r", "</br>").Replace("\a", "").Replace("\r\a", "</br>").Replace("\r\r", "</br>");
                        inhalt = (inhalt.StartsWith("</br>") ? inhalt.Substring(5, inhalt.Length - 5) : inhalt);
                        inhalt = (inhalt.EndsWith("</br>") ? inhalt.Substring(0, inhalt.Length - 5) : inhalt);
                        inhalt = (inhalt.EndsWith("</br>") ? inhalt.Substring(0, inhalt.Length - 5) : inhalt);
                        inhalt = (inhalt.EndsWith("</br>") ? inhalt.Substring(0, inhalt.Length - 5) : inhalt);
                       
                        foreach (var l in links)
                        {
                            if (inhalt.ToLower().Contains("oncoo"))
                            {
                                string aa = "";
                            }
                            if (inhalt.ToLower().Contains(l.Name.ToLower()) && !inhalt.ToLower().Contains(">" + l.Name.ToLower() + "<"))
                            {
                                inhalt = inhalt.ToLower().Replace(l.Name.ToLower(), "<b><a href='" + l.Hyperlink + "' target='_blank'>" + l.Name.ToUpper() + "</a></b>");
                            }
                        }
                        
                        // Prüfe für jede gefundene URL, ...

                        foreach (Match item in Regex.Matches(inhalt, @"(http|ftp|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?"))
                        {
                            // Nur wenn eine URL nicht bereits als Anchor formatiert ist, ...

                            if (!IstUrlInAnführungszeichen(inhalt, item))
                            {
                                // ... wird sie zu einem Hyperlink in HTML umgewandelt

                                inhalt = inhalt.ToLower().Replace(item.Value, "<b><a href='" + item.Value + "' target='_blank'>" + item.Value + "</a></b>");
                            }
                        }
                                                
                        inhalt = "<b>" + bildungsgang + "</b>:</br>" + inhalt;
                        
                        Inhalt = inhalt;
                        Zeile = z + 1;
                        Spalte = s + 1;
                        Console.WriteLine("Zeile: " + z + " Spalte: " + s + " " + Inhalt);                                                
                    }                    
                }
            }
        }

        private bool IstUrlInAnführungszeichen(string inhalt, Match item)
        {   
            if (((inhalt.Substring(inhalt.IndexOf(item.Value) + item.Value.Length).Substring(0, 1) == "“" || inhalt.Substring(inhalt.IndexOf(item.Value) + item.Value.Length).Substring(0, 1) == "'") && (inhalt.Substring(inhalt.IndexOf(item.Value) - 1, 1) == "“") || inhalt.Substring(inhalt.IndexOf(item.Value) - 1, 1) == "'"))
            {
                return true;
            }
            return false;
        }
    }
}